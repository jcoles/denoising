from PIL import Image
import numpy as np
from random import random
from sys import argv
filename = argv[1]
noiseratio = float(argv[2])
im = Image.open(filename)
pix = im.load()
mats = np.matrix([[int(pix[i,j][0]) for i in range(im.size[0])] for j in range(im.size[1])])

for i in range(mats.shape[0]):
    for j in range(mats.shape[1]):
        if random() < noiseratio:
            mats[i,j] = 255-mats[i,j]


#print(np.array([mats,mats,mats]).shape)

noisy_image = Image.fromarray(mats.astype("uint8"))
noisy_image.save(filename+"-noisy.png")


