#todo: machine learn about graph cuts, extend this work to grayscale and color images
from PIL import Image
import numpy as np
from sys import argv
filename = argv[1]
im = Image.open(filename)
pix = im.load()
y = np.matrix([[pix[i,j] for i in range(im.size[0])] for j in range(im.size[1])])

x = np.copy(y)


#Hyperparameters
h = 0
b = 1.0
n = 2.1
numiters = 1

def energy(h,b,n,x,y):
    hterm = 0.0
    bterm = 0.0
    nterm = 0.0

    for i in range(x.shape[0]):
        for j in range(x.shape[1]):
            hterm += 1 if x[i,j] else -1
            #print(bterm,nterm)
            if i > 0:
                bterm += 1 if x[i-1,j]==x[i,j] else -1
            if i < x.shape[0]-1:
                bterm += 1 if x[i+1,j]==x[i,j] else -1
            if j > 0:
                bterm += 1 if x[i,j-1]==x[i,j] else -1
            if j < x.shape[1]-1:
                bterm += 1 if x[i,j+1]==x[i,j] else -1
            nterm += 1 if x[i,j]==y[i,j] else -1

    return hterm*h-bterm*b-nterm*n

for n_ in range(numiters):
    for i in range(x.shape[0]):
        for j in range(x.shape[1]):
            hterm_old = 1 if x[i,j] else -1
            nterm_old = 1 if x[i,j]==y[i,j] else -1
            bterm_old = 0.0
            if i > 0:
                bterm_old += 1 if x[i-1,j]==x[i,j] else -1
            if i < x.shape[0]-1:
                bterm_old += 1 if x[i+1,j]==x[i,j] else -1
            if j > 0:
                bterm_old += 1 if x[i,j-1]==x[i,j] else -1
            if j < x.shape[1]-1:
                bterm_old += 1 if x[i,j+1]==x[i,j] else -1

            hterm_new = -hterm_old
            nterm_new = 1 if x[i,j]!=y[i,j] else -1
            bterm_new = 0.0
            if i > 0:
                bterm_new += 1 if x[i-1,j]!=x[i,j] else -1
            if i < x.shape[0]-1:
                bterm_new += 1 if x[i+1,j]!=x[i,j] else -1
            if j > 0:
                bterm_new += 1 if x[i,j-1]!=x[i,j] else -1
            if j < x.shape[1]-1:
                bterm_new += 1 if x[i,j+1]!=x[i,j] else -1

            if hterm_old*h-bterm_old*b-nterm_old*n > hterm_new*h-bterm_new*b-nterm_new*n:
                x[i,j] = 255-x[i,j]
    print(energy(h,b,n,x,y))
denoised_image = Image.fromarray(x.astype("uint8"))
denoised_image.save(filename+"-denoised.png")
